package com.atguigu.gulimall.thirdparty.component;

import com.alibaba.fastjson.JSONObject;
import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.exceptions.ServerException;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@ConfigurationProperties(prefix = "spring.alicloud.sms")
@Data
@Component
public class SmsComponent {

    private String regionId;

    private String accessKeyId;

    private String accessSecret;

    private String signName;

    private String templateCode;

    private String sysDomain;

    private String sysVersion;

    private String sysAction;

    public void sendSmsCode(String phone,String code)
    {
        Map<String,Object> param = new HashMap<>();
        param.put("code",code);
        DefaultProfile profile = DefaultProfile.getProfile(regionId, accessKeyId, accessSecret);//自己账号的AccessKey信息
        IAcsClient client = new DefaultAcsClient(profile);

        CommonRequest request = new CommonRequest();
        request.setSysMethod(MethodType.POST);
        request.setSysDomain(sysDomain);//短信服务的服务接入地址
        request.setSysVersion(sysVersion);//API的版本号
        request.setSysAction(sysAction);//API的名称
        request.putQueryParameter("PhoneNumbers", phone);//接收短信的手机号码
        request.putQueryParameter("SignName", signName);//短信签名名称
        request.putQueryParameter("TemplateCode", templateCode);//短信模板ID
        request.putQueryParameter("TemplateParam", JSONObject.toJSONString(param));//短信模板变量对应的实际值
        try {
            CommonResponse response = client.getCommonResponse(request);
            System.out.println(response.getData());
        } catch (ServerException e) {
            e.printStackTrace();
        } catch (ClientException e) {
            e.printStackTrace();
        }
    }
}
