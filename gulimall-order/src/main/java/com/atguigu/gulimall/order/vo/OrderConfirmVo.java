package com.atguigu.gulimall.order.vo;

import com.sun.org.apache.xpath.internal.operations.Bool;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public class OrderConfirmVo {

    List<MemberAddressVo> memberAddressVos;

    List<OrderItemVo> items;

    Integer integration;

    //订单总额
    BigDecimal total;

    BigDecimal payPrice;

    String orderToken;

    Map<Long, Boolean> stocks;

    public Map<Long, Boolean> getStocks() {
        return stocks;
    }

    public void setStocks(Map<Long, Boolean> stocks) {
        this.stocks = stocks;
    }

    public Integer getCount()
    {
        Integer i  = 0;
        if(items != null)
        {
            for (OrderItemVo item : items) {
                i += item.getCount();
            }
        }
        return i;
    }

    public List<MemberAddressVo> getMemberAddressVos() {
        return memberAddressVos;
    }

    public void setMemberAddressVos(List<MemberAddressVo> memberAddressVos) {
        this.memberAddressVos = memberAddressVos;
    }

    public List<OrderItemVo> getItems() {
        return items;
    }

    public void setItems(List<OrderItemVo> items) {
        this.items = items;
    }

    public Integer getIntegration() {
        return integration;
    }

    public void setIntegration(Integer integration) {
        this.integration = integration;
    }

    public BigDecimal getTotal() {
        BigDecimal total = new BigDecimal("0");
        if(items != null)
        {
            for (OrderItemVo item : items) {
                BigDecimal multiply = item.getPrice().multiply(new BigDecimal(item.getCount().toString()));
                total = total.add(multiply);
            }
        }
        return total;
    }


    public BigDecimal getPayPrice() {
        BigDecimal total = new BigDecimal("0");
        if(items != null)
        {
            for (OrderItemVo item : items) {
                BigDecimal multiply = item.getPrice().multiply(new BigDecimal(item.getCount().toString()));
                total = total.add(multiply);
            }
        }
        return total;
    }

    public String getOrderToken() {
        return orderToken;
    }

    public void setOrderToken(String orderToken) {
        this.orderToken = orderToken;
    }
}
