package com.atguigu.gulimall.search.service;

import com.atguigu.common.to.es.SkuEsMoudel;

import java.io.IOException;
import java.util.List;

public interface ProductSaveService {
    boolean productStatusUp(List<SkuEsMoudel> skuEsMoudel) throws IOException;
}
