/**
  * Copyright 2021 bejson.com 
  */
package com.atguigu.gulimall.product.vo;

import lombok.Data;

/**
 * Auto-generated: 2021-09-26 1:51:52
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
@Data
public class Attr {

    private Long attrId;
    private String attrName;
    private String attrValue;

}