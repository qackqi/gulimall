package com.atguigu.gulimall.product.vo;

import com.atguigu.gulimall.product.entity.SkuImagesEntity;
import com.atguigu.gulimall.product.entity.SkuInfoEntity;
import com.atguigu.gulimall.product.entity.SpuInfoDescEntity;
import lombok.Data;

import java.util.List;

@Data
public class SkuItemVo {

    SkuInfoEntity info;

    boolean hasStock = true;

    List<SkuImagesEntity> images;

    List<SkuItemSaleAttrVo> saleAttr;

    SpuInfoDescEntity desc;

    List<SpuItemAttrGroupVo> groupAttrs;

    @Data
    public static class SkuItemSaleAttrVo
    {

        private Long attrId;

        private String attrName;

        private List<AttrValueWithSkuIdVo> attrValues;
    }

    @Data
    public static class SpuItemAttrGroupVo
    {
        private String groupName;

        private List<SpuBaseAttrVo> attrs;
    }

    @Data
    public static class SpuBaseAttrVo
    {
        private String attrName;

        private String attrValue;
    }

    @Data
    public static class AttrValueWithSkuIdVo
    {
        private String attrValue;

        private String skuIds;
    }
}
