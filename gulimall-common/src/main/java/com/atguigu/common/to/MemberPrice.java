/**
  * Copyright 2021 bejson.com 
  */
package com.atguigu.common.to;

import lombok.Data;

import java.math.BigDecimal;

/**
 * Auto-generated: 2021-09-26 1:51:52
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
@Data
public class MemberPrice {

    private Long id;
    private String name;
    private BigDecimal price;

}