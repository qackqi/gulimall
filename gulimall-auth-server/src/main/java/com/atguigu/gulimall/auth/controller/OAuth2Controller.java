package com.atguigu.gulimall.auth.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.atguigu.common.utils.HttpUtils;
import com.atguigu.common.utils.R;
import com.atguigu.gulimall.auth.feign.MemberFeignService;
import com.atguigu.common.vo.MemberRespVo;
import com.atguigu.gulimall.auth.vo.SocialUser;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;

@Controller
public class OAuth2Controller {

    @Autowired
    MemberFeignService memberFeignService;

    @GetMapping("/oauth2.0/weibo/success")
    public String weibo(@RequestParam("code") String code, HttpSession session) throws Exception {
        Map<String,String> map = new HashMap<>();
        Map<String,String> headers = new HashMap<>();
        Map<String,String> query = new HashMap<>();
//        headers.put("Host","api.weibo.com");
        map.put("client_id","2972998234");
        map.put("client_secret","96d3c7657bb628d275c319b4388666a1");
        map.put("grant_type","authorization_code");
        map.put("redirect_uri","http://auth.gulimall.com/oauth2.0/weibo/success");
        map.put("code",code);
        HttpResponse response = HttpUtils.doPost("https://api.weibo.com", "/oauth2/access_token", "post", headers, map, query);
        //处理
        if(response.getStatusLine().getStatusCode() == 200)
        {
            //获取到accessToken
            String json = EntityUtils.toString(response.getEntity());
            SocialUser socialUser = JSON.parseObject(json, SocialUser.class);
            R oauthLogin = memberFeignService.oauthLogin(socialUser);
            if(oauthLogin.getCode() == 0)
            {
                MemberRespVo data = oauthLogin.getData("data", new TypeReference<MemberRespVo>() {
                });
                System.out.println("登陆成功");
                session.setAttribute("loginUser",data);
                return "redirect:http://gulimall.com";
            }
            else {
                return "redirect:http://auth.gulimall.com/login.html";
            }

        }
        else
        {
            return "redirect:http://auth.gulimall.com/login.html";
        }

    }
}
